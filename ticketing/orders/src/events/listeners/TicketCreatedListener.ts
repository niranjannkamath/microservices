import { Message } from 'node-nats-streaming';
import { Subjects, Listener, TicketCreatedEvent } from '@microservices_course_niranjan/common';
import { Ticket } from '../../models/ticket';
import { queueGroupName } from './QueueGroupName';

export class TicketCreatedListener extends Listener<TicketCreatedEvent> {
    readonly subject = Subjects.TICKET_CREATED;
    queueGroupName = queueGroupName;

    async onMessage(data: TicketCreatedEvent['data'], msg: Message) {
        const { id, title, price } = data;
        const ticket = Ticket.build({
            id, title, price
        });
        await ticket.save();

        msg.ack();
    }
}