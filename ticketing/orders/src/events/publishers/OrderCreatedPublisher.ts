import { Publisher, OrderCreatedEvent, Subjects } from '@microservices_course_niranjan/common';

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
    readonly subject = Subjects.ORDER_CREATED;
}