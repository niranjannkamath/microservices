import { Publisher, OrderCancelledEvent, Subjects } from '@microservices_course_niranjan/common';

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
    readonly subject = Subjects.ORDER_CANCELLED;
}