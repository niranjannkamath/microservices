import { Subjects } from './Subjects';

export interface ExpirationCompleteEvent {
    subject: Subjects.EXPIRATION_COMPLETE;
    data: {
        orderId: string;
    };
}