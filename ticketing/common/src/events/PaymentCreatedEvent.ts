import { Subjects } from './Subjects';

export interface PaymentCreatedEvent {
    subject: Subjects.PAYMENT_CREATED;
    data: {
        id: string;
        orderId: string;
        stripeId: string;
    }
}