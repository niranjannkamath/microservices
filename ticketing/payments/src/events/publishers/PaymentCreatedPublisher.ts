import { PaymentCreatedEvent, Publisher, Subjects } from "@microservices_course_niranjan/common";

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
    readonly subject = Subjects.PAYMENT_CREATED;
}