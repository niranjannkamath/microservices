import { Publisher, Subjects, TicketCreatedEvent } from '@microservices_course_niranjan/common';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
    readonly subject = Subjects.TICKET_CREATED;
}