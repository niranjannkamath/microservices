import { Publisher, Subjects, TicketUpdatedEvent } from '@microservices_course_niranjan/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
    readonly subject = Subjects.TICKET_UPDATED;
}