import { Listener, OrderCreatedEvent, Subjects } from '@microservices_course_niranjan/common';
import { Message } from 'node-nats-streaming';
import { queueGroupName } from './QueueGroupName';
import { expirationQueue } from '../../queues/ExpirationQueue';

export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
    readonly subject = Subjects.ORDER_CREATED;
    queueGroupName = queueGroupName;

    async onMessage(data: OrderCreatedEvent['data'], msg: Message) {
        const delay = new Date(data.expiresAt).getTime() - new Date().getTime();
        console.log('Waiting this many milliseconds to process a job', delay);

        await expirationQueue.add({
            orderId: data.id
        }, {
            delay: delay,
        });

        msg.ack();
    }
}