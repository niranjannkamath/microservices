import { Publisher, ExpirationCompleteEvent, Subjects } from '@microservices_course_niranjan/common';

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
    readonly subject = Subjects.EXPIRATION_COMPLETE;
}